board=[["_", "_","_","_","_", "_", "_", "_", "_", "_"],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ]]

board2=[["_", "_","_","_","_", "_", "_", "_", "_", "_"],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ]]
hBoard=[["_", "_","_","_","_", "_", "_", "_", "_", "_"],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ]]

hBoard2=[["_", "_","_","_","_", "_", "_", "_", "_", "_"],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ],
      ["_", "_","_","_","_", "_", "_", "_", "_", "_" ]]
def printBoard(board):
    for c in range(0,10):
        for x in range(0,10):
            print("|", end="")
            print(board[c][x], end="")
        print("|") 
        
def place(board, row, column, rotation, size):
    if rotation=="horizontally" or rotation=="H":
        for x in range(column,column+size):
            board[row][x]="O"       
    if rotation=="vertically" or rotation=="V":
        for x in range(row,row+size):
            board[x][column]="O"
    return(board)      
def ask(board, size, ship):       
    move=0
    while move==0:
        piece=int(input("Which row would you like to place your "+str(ship)+" battleship?"))-1
        piece2=int(input("Which column would you like to place your "+str(ship)+" battleship?"))-1
        piece3=input("How would you like to rotate your shape, horizontally or vertically?")
        clearance=0
        if piece3== "Horizontally" or piece3=="H" :
            if piece2+size>10:
                clearance=1
            else:
                for c in range(piece2,piece2+size):
                    if board[piece][c]!="_":
                        clearance=clearance+1

        if piece3=="Vertically"  or piece3=="V":
            if piece+size>10:
                clearance=1
            else:
                for c in range(piece,piece+size):
                    if board[c][piece2]!="_":
                        clearance=clearance+1
        if board[piece][piece2]=="_" and clearance==0: 
            board=place(board, piece, piece2, piece3, size)
            printBoard(board)
            move=1
        else:
            print("Space taken")
        
def docking(board):
    input("Press enter to place your pieces.")
    ask(board, 2, "2x1")
    ask(board, 3, "first 3x1")
    ask(board, 3, "second 3x1")
    ask(board, 4, "4x1")
    ask(board, 5, "5x1")
    
def attack(board,hBoard):
    targetrow=int(input("Which row would you like to attack"))
    targetcolumn=int(input("Which column would you like to attack"))
    if(board[targetrow][targetcolumn]!="_"):
        hBoard[targetrow][targetcolumn]="X"
        print("You've hit and damaged the ship")
    else:
        hBoard[targetrow][targetcolumn]="0"        
    return(hBoard)

def winCheck(board, player):
    counter=0
    for z in range(0,10):
        for t in range(0,10):
            if board[t][z]!="_":
                counter=counter+1
    if counter==17:
        return(player)
    else:
        return("No one")
print("player one")
printBoard(board)
docking(board)
print("player two")
printBoard(board2)

docking(board2)

winner="No one"
while winner=="No one":
    print()
    print()
    print()
    print()
    print("Player One, it's your turn to attack.")
    printBoard(hBoard)
    hBoard=attack(board2, hBoard)
    printBoard(hBoard)
    winner=winCheck(hBoard, "player 1")
    input("press enter to continue")
    print()
    print()
    print()
    print()
    print("Player Two, it's your turn to attack.")
    printBoard(hBoard2)
    hBoard2=attack(board,hBoard2)
    printBoard(hBoard2)
    winner=winCheck(hBoard2, "player 2")
    input("press enter to continue")
    
if winner=="player 1":
    print("Player One rules the seas!!!")
if winner=="player 2`":
    print("Player Two owns the master navy!!!")    