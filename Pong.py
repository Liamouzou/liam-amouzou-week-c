import msvcrt
import time
import os
def gross():
    kb=msvcrt.kbhit()
    if kb:
        ret=msvcrt.getch()
    else:
        ret=False
    return(ret)
player1=5
player2=5
board=[]
row=[]
ball=[8,7,1,0]
x=0
for x in range (0,15):
    row.append(" ")
for x in range(0,15):
    board.append(row.copy())
    
def moveplayer1(player, board):
    for g in range(0,15):
        for b in range(0,15):
            if b==0 and g<player+5 and g>=player:
                board[g][b]="]"
            elif b==0:
                board[g][b]=" "
    return(board)


def moveplayer2(player, board):
    for g in range(0,15):
        for b in range(0,15):
            if b==14 and g<player+5 and g>=player:
                board[g][b]="["  
            elif b==14:
                board[g][b]=" "
    return(board)


def printBoard(board):
    os.system("cls")
    print("==============================")
    for x in range(0,15):
        for c in range(0,15):
            print(board[x][c], end="")
            print(" ", end="")
        print()  
    print("==============================")
            
def moveball(ball):
    if ball[1]==0 or ball[1]==14:
        ball[3]=-ball[3]
    board[ball[1]][ball[0]]=" "
    ball[0]=ball[0]+ball[2]
    ball[1]=ball[1]+ball[3]
    board[ball[1]][ball[0]]="■"
    return(ball)

def paddle(ball, player, pos):
    if ball[0]==pos and ball[1]==player+2:
        ball[2]=-ball[2]
    elif ball[0]==pos and ball[1]==player+1:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=-1
        else:
            ball[3]=-ball[3]
    elif ball[0]==pos and ball[1]==player+3:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=1
        else:
            ball[3]=-ball[3]
    elif ball[0]==pos and ball[1]==player+4:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=1
        else:
            ball[3]=-ball[3]+1
    elif ball[0]==pos and ball[1]==player:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=-1
        else:
            ball[3]=-ball[3]-1
    return(ball)
def win(ball,player,pos):
    if ball[0]<0:
        return(1)
    if ball[0]==pos:
        if ball[1]<player or ball[1]>player+4:
            return(pos)
        else:
            return("No one")
    else:
        return("No one")
winner="No one"
while winner=="No one":
    move=gross()
    if move!=False and player1>0 and move.decode("utf-8")=="w" :
        player1=player1-1
    if move!=False and player1<10 and move.decode("utf-8")=="s":
        player1=player1+1
    if move!=False and player2>0 and move.decode("utf-8")=="8" :
        player2=player2-1
    if move!=False and player2<10 and move.decode("utf-8")=="5":
        player2=player2+1
    x=x+1
    speed=3/x    
    ball=moveball(ball)
    ball=paddle(ball, player1, 1)
    winner=win(ball, player1, 1)
    ball=paddle(ball, player2, 14) 
    winner=win(ball, player2, 14)   
    board=moveplayer1(player1, board)
    board=moveplayer2(player2, board)
    printBoard(board)
    time.sleep(speed)
    
if winner==1:
    print("Player 2 wins")
if winner==14:
    print("Player 1 wins") 